﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace Calculator
{
    public partial class MainWindow : Window
    {
        Calculate calculate = new Calculate();

        public MainWindow()
        {
            InitializeComponent();
            txtInput.IsEnabled = false;
        }

        private void Calculate()
        {
            int opPos = 0;
            double value1 = 0;
            double value2 = 0;
            double result = 0;
            string op = "";


            if (txtInput.Text.Contains("*"))
            {
                opPos = txtInput.Text.IndexOf("*");
            }

            if (txtInput.Text.Contains("/"))
            {
                opPos = txtInput.Text.IndexOf("/");
            }

            if (txtInput.Text.Contains("+"))
            {
                opPos = txtInput.Text.IndexOf("+");
            }

            if (txtInput.Text.Contains("-"))
            {
                opPos = txtInput.Text.IndexOf("-");
            }

            if (txtInput.Text.Contains("%"))
            {
                opPos = txtInput.Text.IndexOf("%");
            }

            value1 = Double.Parse(txtInput.Text.Substring(0, opPos));
            op = txtInput.Text.Substring(opPos, 1);
            value2 = Double.Parse(txtInput.Text.Substring(opPos + 1, txtInput.Text.Length - opPos - 1));

            switch (op)
            {
                case "*":
                    {
                        result = calculate.multiply(value1, value2);
                        txtInput.Text = "";
                        txtInput.Text += result.ToString();

                        break;
                    }

                case "/":
                    {
                        if (value2 == 0)
                        {
                            txtInput.Text = "Cannot divide by 0!";
                        }

                        else
                        {
                            result = calculate.divide(value1, value2);
                            txtInput.Text = "";
                            txtInput.Text += result.ToString();
                        }

                        break;
                    }

                case "+":
                    {
                        result = calculate.add(value1, value2);
                        txtInput.Text = "";
                        txtInput.Text += result.ToString();
                        break;
                    }

                case "-":
                    {
                        result = calculate.subtract(value1, value2);
                        txtInput.Text = "";
                        txtInput.Text += result.ToString();
                        break;
                    }

                case "%":
                    {
                        result = this.calculate.percent(value1, value2);
                        txtInput.Text = "";
                        txtInput.Text += result.ToString();
                        break;
                    }
            }
        }

        private void btnEqual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Calculate();
            }
            catch (Exception)
            {
                txtInput.Text = "Error! Try again.";
            }
        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;

            if (button.Content != btnDot.Content)
            {
                if (txtInput.Text.Equals((String)btn0.Content))
                {
                    txtInput.Clear();
                }
            }

            txtInput.Text += button.Content.ToString();
        }

        private void btnCE_Click(object sender, RoutedEventArgs e)
        {
            txtInput.Text = "0";
        }

        private void btnC_Click(object sender, RoutedEventArgs e)
        {
            txtInput.Text = "0";
        }

        private void btnBackspace_Click(object sender, RoutedEventArgs e)
        {
            if (txtInput.Text.Length <= 1)
            {
                txtInput.Text = "0";
            }

            if (txtInput.Text.Length > 1)
            {
                string lastElem = txtInput.Text.Remove(txtInput.Text.Length - 1, 1);
                txtInput.Text = lastElem;
            }
        }

        private void btn1_x_Click(object sender, RoutedEventArgs e)
        {
            double val = 0;
            double rez = 0;
            val = Convert.ToDouble(txtInput.Text);
            rez = calculate.divX(val);
            txtInput.Text = rez.ToString();
        }

        private void btnPow_Click(object sender, RoutedEventArgs e)
        {
            double value = 0;
            double rez = 0;
            value = Convert.ToDouble(txtInput.Text);
            rez = calculate.powOp(value);
            txtInput.Text = rez.ToString();
        }

        private void btnSqrt_Click(object sender, RoutedEventArgs e)
        {
            double val = 0;
            double rez = 0;
            val = Convert.ToDouble(txtInput.Text);
            rez = calculate.sqrtOp(val);
            txtInput.Text = rez.ToString();
        }

        private void btnPositiveNegative_Click(object sender, RoutedEventArgs e)
        {
            double val = 0;
            double rez = 0;
            val = Convert.ToDouble(txtInput.Text);
            rez = calculate.reverse(val);
            txtInput.Text = rez.ToString();
        }
    }
}
