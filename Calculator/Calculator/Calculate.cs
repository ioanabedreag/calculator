﻿using System;


namespace Calculator
{
    class Calculate
    {
        public double add(double a, double b)
        {
            return (a + b);
        }

        public double subtract(double a, double b)
        {
            return (a - b);
        }

        public double multiply(double a, double b)
        {
            return (a * b);
        }

        public double divide(double a, double b)
        {
            return (a / b);
        }

        public double percent(double a, double b)
        {
            return (a % b);
        }

        public double powOp(double x)
        {
            return (x * x);
        }

        public double sqrtOp(double x)
        {
            return (Math.Sqrt(x));
        }

        public double divX(double x)
        {
            return (1 / x);
        }

        public double reverse(double x)
        {
            if (x < 0)
            {
                return x;
            }
            return -x;
        }
    }
}
